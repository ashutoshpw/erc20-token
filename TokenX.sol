// SPDX-Licence-Identifier: MIT
pragma solidity ^0.8.3;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "hardhat/console.sol";

contract TOKENX is Context, IERC20, IERC20Metadata, ReentrancyGuard {
    using SafeMath for uint256;
    mapping(address => uint256) private _balances;
    mapping(address => mapping(address => uint256)) private _allowances;
    mapping(address => bool) private _isExcludedFromFees;

    uint256 private _totalSupply = 10**6 * 10**6 * 10**18; // 10^12 total supply

    string private _name = "TOKENX";
    string private _symbol = "TKX";
    uint8 private _decimals = 18;

    /** Configure wallet before Deployments */
    address private _developersWallet = 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266;
    address private _marketingWallet = 0x8626f6940E2eb28930eFb4CeF49B2d1F2C9C1199; 
    address private _liquidityWallet = 0xdD2FD4581271e230360230F9337D5c0430Bf44C0;
    address private _charityWallet = 0xbDA5747bFD65F08deb54cb465eB87D40e51B197E;
  

    constructor() {

        // Sends 100% of total supply to founder's wallet //
        _balances[_developersWallet] = _totalSupply.mul(100).div(100);

        /** Few Wallets are excluded from the Fees */
        _isExcludedFromFees[address(this)] = true;
        _isExcludedFromFees[_developersWallet] = true;
        _isExcludedFromFees[_liquidityWallet] = true;
        _isExcludedFromFees[_marketingWallet] = true;
        _isExcludedFromFees[_charityWallet] = true;
    }

    modifier onlyOwner() {
        require(
            msg.sender == _developersWallet,
            "Only owner can call this function"
        );
        _;
    }
    

    function excludeAddressFromFees(address excludedAddress) public onlyOwner {
        _isExcludedFromFees[excludedAddress] = true;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function name() public view virtual override returns (string memory) {
        return _name;
    }

    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function decimals() public view virtual override returns (uint8) {
        return _decimals;
    }

    function balanceOf(address account)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _balances[account];
    }

    /**
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    /**
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function approve(address spender, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(
            currentAllowance >= amount,
            "ERC20: transfer amount exceeds allowance"
        );
        _approve(sender, _msgSender(), currentAllowance - amount);

        return true;
    }

    /**
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     */
    function increaseAllowance(address spender, uint256 addedValue)
        public
        virtual
        returns (bool)
    {
        _approve(
            _msgSender(),
            spender,
            _allowances[_msgSender()][spender] + addedValue
        );
        return true;
    }

    /**
     * This is an alternative to {approve} that can be used as a mitigation for
     * problems described in {IERC20-approve}.
     *
     * Emits an {Approval} event indicating the updated allowance.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `spender` must have allowance for the caller of at least
     * `subtractedValue`.
     */
    function decreaseAllowance(address spender, uint256 subtractedValue)
        public
        virtual
        returns (bool)
    {
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(
            currentAllowance >= subtractedValue,
            "ERC20: decreased allowance below zero"
        );
        _approve(_msgSender(), spender, currentAllowance - subtractedValue);

        return true;
    }

    /**
     * This internal function is equivalent to {transfer}, and can be used to
     * e.g. implement automatic token fees, slashing mechanisms, etc.
     *
     * Emits a {Transfer} event.
     *
     * Requirements:
     *
     * - `sender` cannot be the zero address.
     * - `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     */

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");
        uint256 senderBalance = _balances[sender];
        require(
            senderBalance >= amount,
            "ERC20: transfer amount exceeds balance"
        );

        if (_isExcludedFromFees[recipient] || _isExcludedFromFees[sender]) {
            _beforeTokenTransfer(sender, recipient, amount);
            _balances[sender] = senderBalance - amount;
            _balances[recipient] += amount;
            emit Transfer(sender, recipient, amount);
            _afterTokenTransfer(sender, recipient, amount);
        }
        else {
            _beforeTokenTransfer(sender, recipient, amount);
            (
                uint256 _marketingCommission,
                uint256 _liquidityCommission,
                uint256 _charityCommission
            ) = calculateCommissions(amount);

            uint256 platformCharges = _marketingCommission +
                _liquidityCommission +
                _charityCommission;

            require(
                senderBalance >= (amount + platformCharges),
                "ERC20: transfer amount exceeds balance"
            );
            _balances[sender] = senderBalance - amount - platformCharges;
            _balances[_marketingWallet] += _marketingCommission;
            _balances[_liquidityWallet] += _liquidityCommission;
            _balances[_charityWallet] += _charityCommission;
            _balances[recipient] += amount;
            emit Transfer(sender, recipient, amount);
            _afterTokenTransfer(sender, recipient, amount);
        }
    }

    function cutPer10000(uint256 _cut, uint256 _total)
        internal
        pure
        returns (uint256)
    {
        uint256 cutAmount = _total.mul(_cut).div(10000);
        return cutAmount;
    }

    function calculateCommissions(uint256 _amount)
        internal
        pure
        returns (
            uint256 _marketingCommission,
            uint256 _liquidityCommission,
            uint256 _charityCommission
        )
    {
        _marketingCommission = cutPer10000(1 * 100, _amount); //1% of total
        _liquidityCommission = cutPer10000(1.5 * 100, _amount); //1.5% of total
        _charityCommission = cutPer10000(1.5 * 100, _amount); //1.5% of total
        return (_marketingCommission, _liquidityCommission, _charityCommission);
    }

    function mint(address account, uint256 amount) public onlyOwner {
        _mint(account, amount);
    }

    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: mint to the zero address");
        _beforeTokenTransfer(address(0), account, amount);
        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);
        _afterTokenTransfer(address(0), account, amount);
    }

    function burn(uint256 amount) public onlyOwner {
        _burn(address(this), amount);
    }

    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: burn from the zero address");
        _beforeTokenTransfer(account, address(0), amount);
        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount, "ERC20: burn amount exceeds balance");
        _balances[account] = accountBalance - amount;
        _totalSupply -= amount;
        emit Transfer(account, address(0), amount);
        _afterTokenTransfer(account, address(0), amount);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    receive() external payable {}
}